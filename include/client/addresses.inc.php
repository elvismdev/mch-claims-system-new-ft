<?php
if(!defined('OSTCLIENTINC')) die('Access Denied!');

if (isset($errors['warranty'])) {
    echo '<div  class="expnotice"><p>Thank you for contacting our warranty support center.
	<br /><br />
     Our records indicate that your warranty with Mountain Cove Homes has expired. Your home is covered under our warranty for a period of (1) year from your closing date.If you are receiving this message and are currently within your warranty period, please contact us directly by calling at (786) 458-1805 or sending an email to <a href="mailto:info@mountaincovehomes.com">info@mountaincovehomes.com</a>. In your email, please detail your name, address, contact information and a brief explanation of the issue you are having with your home.</p></div>';
    exit;
}

$form_ids = array_keys($_POST);
$_SESSION['claim_post'] = $_POST;
?>

<table class="rowhover">
	<thead>
		<tr>
			<th><h2>Property Street Address</h2></th>
            <th><h2>City</h2></th>
            <th><h2>State</h2></th>
            <th><h2>ZIP</h2></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($addrrs as $a) { ?>
		<tr>
			<td> <?php echo '<a href="?ad='.$a['id'].'&id='.$form_ids[5].'">'.$a['address'].'</a>' ?> </td>
            <td> <?php echo '<a href="?ad='.$a['id'].'&id='.$form_ids[5].'">'.$a['city'].'</a>' ?> </td>
            <td> <?php echo '<a href="?ad='.$a['id'].'&id='.$form_ids[5].'">'.$a['state'].'</a>' ?> </td>
            <td> <?php echo '<a href="?ad='.$a['id'].'&id='.$form_ids[5].'">'.$a['zip'].'</a>' ?> </td>
		</tr>
		<?php } ?>
	</tbody>
</table>